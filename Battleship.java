
import java.util.Scanner;

public class Battleship {
    public static void main(String[] args) {

        String boat = "                                     |__" + "\n" + "                                     |\\/"
                + "\n" + "                                     ---" + "\n"
                + "                                     / | [" + "\n" + "                              !      | |||"
                + "\n" + "                            _/|     _/|-++'" + "\n"
                + "                        +  +--|    |--|--|_ |-" + "\n"
                + "                     { /|__|  |/\\__|  |--- |||__/" + "\n"
                + "                    +---------------___[}-_===_.'____               /\\" + "\n"
                + "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--          \\/   _" + "\n"
                + " __..._____--==/___]_|__|_____________________________[___\\==--___,-----' .7" + "\n"
                + "|                                                                SS-YouSuck/" + "\n"
                + " \\_______________________________________________________________________|" + "\n"
                + "  Matthew Bace";

        // Print out this awesome boat
        System.out.println(boat);

        // Define some variables for the game including board size
        
        

        // Make all the ships
        Ship ship1 = new Ship("Caryr", "A1,A2,A3,A4,A5", 5);
        Ship ship2 = new Ship("Battl", "D6,E6,F6,G6", 4);
        Ship ship3 = new Ship("Dstry", "I2,I3,I4", 3);
        Ship ship4 = new Ship(" Sub ", "B8,B9,B10", 2);
        Ship ship5 = new Ship("Patrl", "I10", 1);

        // Add ships to array for setting placement
        Ship[] ships = { ship1, ship2, ship3, ship4, ship5 };

        Board battleBoard = new Board();
        battleBoard.placeShips(ships);
        battleBoard.print();
        battleBoard.fireShot();
    }
}